## Functions:
- Simple folders navigation
- Press *space* to see some information about selected folder

This is a exsample **Web App** and it has purpose to demonstrate how realize applications using web tecnologies has become an easy way to develop any kind of cross platform software.

# Why choose a Web App?

There are plenty of alternatives to accomplish the same thing differently.
  Which one can be the best for your needs?  
- The **cheapest**?
- The most **captivating**?  
- The **fastest** to make?  
- The most **performing**?
- Or simply continue to use your old software and adjust only the graphical interface?

Thank you for trying this sample **App**.
Visit [matteocasagrande.com](http://www.matteocasagrande.com) for more information.

Realized by [*BigH*](http://www.matteocasagrande.com) 