## Funzioni:  
- Semplice navigazione tra le cartelle
- Tasto *space* per visualizzare alcune informazioni della cartella selezionata

Questa è una **Web App** d'esempio ed ha lo scopo di dimostrare come la realizzazione di applicazioni attraverso tecnologie web sia diventato un modo semplice, efficace e cross platform per sviluppare software di ogni genere.

# Perché scegliere una Web App?

Ci sono un'infinità di alternative per realizzare la stessa cosa in modo differente.  
Quale può essere quella più adatta alle tue esigenze?  
- La più **economica**?
- La più **accattivante**?  
- La più **veloce** da realizzare?  
- La più **performante**?
- O semplicemente continuare ad utilizzare il tuo vecchio software e adeguare esclusivamente l'interfaccia grafica?

Grazie per aver provato questa **App** di esempio. 
Visita la pagina [matteocasagrande.com](http://www.matteocasagrande.com) per maggiori informazioni.

Realized by [*BigH*](http://www.matteocasagrande.com) 