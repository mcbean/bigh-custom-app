/**
 * Created by Matteo Casagrande on 26/10/16.
 */

// Open links externally by default
$(document).on('click', 'a[href^="http"]', function(event) {
    event.preventDefault();
    _SHELL.openExternal(this.href);
});