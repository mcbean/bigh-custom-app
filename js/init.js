/**
 * Created by Matteo Casagrande on 28/10/16.
 */

const _REMOTE = require('electron').remote;
const _FS = require('fs');
const _OS = require('os');
const _PATH = require('path');
const _GETSIZE = require('get-folder-size');
const _SHELL = _REMOTE.shell;
const Highcharts = require('highcharts');

//const _MENU = _REMOTE.Menu;
/*const _DISK = {
    space: require('diskspace'),
    usage: require('diskusage')
};*/

//console.log(Buffer);
//console.log(_PATH);
//console.log(_OS.homedir());

$("#header").on("click",".brand",function(e){
    $("#appInfo").modal("show");
});

// TODO: Ad function to BigH namespace
function byteToStr(fBytes){

    if(fBytes < 1000){
        return fBytes.toFixed(2) + ' b'
    }

    //fBytes = fBytes / 1024;
    fBytes = fBytes / 1000;

    if(fBytes < 1000){
        return fBytes.toFixed(2) + ' kb'
    }

    //fBytes = fBytes / 1024;
    fBytes = fBytes / 1000;

    if(fBytes < 1000){
        return fBytes.toFixed(2) + ' Mb'
    }

    //fBytes = fBytes / 1024;
    fBytes = fBytes / 1000;

    return fBytes.toFixed(2) + ' Gb'

    /*if(fBytes < 1000){
        return fBytes.toFixed(2) + ' Gb'
    }*/

};

//Append Home path to data
var strHomeDir = _OS.homedir();
var strHomeSpace = "";

// Home folder
/*_GETSIZE(strHomeDir, function(err, size) {
    if (err){
        throw err;
    }

    //console.log(size + ' bytes');
    //console.log((size / 1024 / 1024).toFixed(2) + ' Mb');

    //strHomeSpace = (size / 1024 / 1024).toFixed(2) + ' Mb';
    strHomeSpace = byteToStr(size);

});*/


//var strHomeDir = "/Users/apple/Documents";
$("#level-1 .item").data("path",strHomeDir);

/*_FS.readdir(strHomeDir, (err, files) => {
    files.forEach(file => {
    console.log(file);
});
});*/

/** Recursive read dir content
 * @param String contains path
 * @return Array of objects contains dir name and path
 */

function fReadDir($elm){
    var strPath = $elm.data("path");
    var $level = $elm.parent(".level");
    var $nextLevel = $level.next();
    var aContent = [];

    // Remove selection and next level content
    $nextLevel.empty().nextAll(".level").remove();
    $level.find(".selected").removeClass("selected");

    $elm.addClass("selected");

    _FS.readdir(strPath, function(err, aFiles){
        if (err) {
            console.error(err);
            throw err;
        }

        //console.warn(aFiles);

        for(var i=0; i<aFiles.length; i++){

            var sName = aFiles[i];
            var sPath = strPath + _PATH.sep + sName;

            //console.warn(_FS.lstatSync(strHomeDir).isDirectory());
            //console.log("%s (%s)", aFiles[i], _PATH.extname(aFiles[i]));
            //console.warn(_FS.lstatSync(strPath + _PATH.sep + aFiles[i]).isDirectory());

            if(_FS.lstatSync(sPath).isDirectory()){
                //console.warn(sPath);

                aContent.push({
                    name: sName,
                    path: sPath
                })
            }
        }

        /*aFiles.filter(function (file) {
            return _FS.statSync(file).isFile();
        }).forEach(function (file) {
            console.log("%s (%s)", file, _PATH.extname(file));
        });*/

        // Create next level if not exist
        if($nextLevel.length < 1){
            $nextLevel = $('<ul class="level">');
            $level.after($nextLevel);
        }

        // Add folders to level
        for(var i=0; i<aContent.length; i++){
            var $item = $('<li class="item"><div class="folder"></div><span class="name">'+ aContent[i].name +'</span></li>');
            //$item.attr("title",aContent[i].name);
            $item.attr("title",aContent[i].path);

            $item.data("path",aContent[i].path)

            $nextLevel.append($item);
        }

        // Scroll to bottom of document
        //$("html, body").animate({ scrollTop: $(document).height() }, 500);
        //window.scrollTo(0,document.body.scrollHeight);
    });
};

// Show directory content
$(".wizard-page").on("click",".item",function(e){
    var $this = $(this);

    if($this.hasClass("selected") && $this.is("selected:last"))
        return;

    //console.log($this.data("path"));

    // Build folder content level
    fReadDir($this);
});

// Space key - App info
$('body').keyup(function(e){

    var $selFolders = $(".selected");
    var $selected = $selFolders.last();

    if($selected.find(".folder").hasClass("folder-home")){
        return;
    }

    // Parent folder
    /*_GETSIZE(_PATH.resolve($selected.data("path"),".."), function(err, size) {
        if (err) { throw err; }

        //console.log(size + ' bytes');
        //console.log((size / 1024 / 1024).toFixed(2) + ' Mb');

        $modalInfo.find(".modal-parent-space").text((size / 1024 / 1024).toFixed(2) + ' Mb');

    });

    // Home folder
    _GETSIZE(strHomeDir, function(err, size) {
        if (err) { throw err; }

        //console.log(size + ' bytes');
        console.log((size / 1024 / 1024).toFixed(2) + ' Mb');

        //$modalInfo.find(".modal-folder-home").text((size / 1024 / 1024).toFixed(2) + ' Mb');

    });*/

    //$modalInfo.find(".modal-title").html($selected.data("path"));

    //Toggle modal info-window
    if(e.keyCode == 32){
        // user has pressed space
        $('#myModal').modal('toggle');
    }
});

var myChart = Highcharts.chart('myChart', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        width: 568
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    title: {
        text: null
    },
    series: [{
        data: [{
            name: 'Microsoft Internet Explorer',
            y: 56.33
        }, {
            name: 'Chrome',
            y: 24.03
        }, {
            name: 'Firefox',
            y: 10.38
        }, {
            name: 'Safari',
            y: 4.77
        }, {
            name: 'Opera',
            y: 0.91
        }, {
            name: 'Proprietary or Undetectable',
            y: 0.2
        }]
    }]
});

$('#myModal').on('show.bs.modal', function (e) {
    var $this = $(this);
    var $selFolders = $(".selected");
    var $selected = $selFolders.last();

    if($selFolders.length < 1){
        return;
    }

    // Pats
    var strSelFolder = $selected.data("path");
    //var strParentFolder = _PATH.resolve($selected.data("path"),"..");

    $this.find(".modal-path").html(strSelFolder);
    $this.find(".modal-title").text($selected.find(".name").text());



    // Selected folder
    _GETSIZE(strSelFolder, function(err, size) {
        if (err){
            throw err;
        }

        console.log(size + ' bytes');
        //console.log((size / 1024 / 1024).toFixed(2) + ' Mb');

        $this.find(".modal-folder-space").removeClass("loader").html(byteToStr(size));

    });

    // Draw chart
    _FS.readdir(strSelFolder, function(err, aFiles){
        if (err) {
            console.error(err);
            throw err;
        }

        var iDOC = 0;
        var iIMG = 0;
        var iDIR = 0;
        var iOTH = 0;
        var iMUS = 0;

        for(var i=0; i<aFiles.length; i++){

            var sName = aFiles[i];
            var sExt = _PATH.extname(aFiles[i]).toLowerCase();
            var sPath = strSelFolder + _PATH.sep + sName;

            switch (sExt){
                case ".csv":
                case ".pdf":
                case ".doc":
                case ".docx":
                case ".odt":
                case ".ods":
                case ".odg":
                case ".odp":
                case ".pps":
                case ".txt":
                case ".tex":
                case ".ltx":
                case ".rtf":
                case ".xls":
                case ".xlsx":
                    iDOC++;
                    break
                case ".mp3":
                case ".mid":
                case ".midi":
                case ".acc":
                case ".ac3":
                case ".wma":
                    iMUS++;
                    break
                case ".gif":
                case ".bmp":
                case ".cdp":
                case ".eps":
                case ".gdp":
                case ".png":
                case ".pict":
                case ".jpg":
                case ".jpeg":
                case ".jpd":
                case ".tiff":
                case ".tga":
                case ".pcx":
                case ".psd":
                    iIMG++;
                    break
                default:
                    if(_FS.lstatSync(strSelFolder + _PATH.sep + aFiles[i]).isDirectory()){
                        iDIR++;
                    }
                    else{
                        iOTH++;
                    }
            }

            //console.warn(_FS.lstatSync(strHomeDir).isDirectory());
            console.log("%s (%s)", aFiles[i], _PATH.extname(aFiles[i]));
            //console.warn(_FS.lstatSync(strPath + _PATH.sep + aFiles[i]).isDirectory());
        }

        myChart.series[0].setData([{
                color: "#333333",
                name: "Cartelle",
                y: iDIR
            },
            {
                color: "#FFF434",
                name: "Immagini",
                y: iIMG
            },
            {
                color: "#7ACC98",
                name: "Musica",
                y: iMUS
            },
            {
                color: "#FF7B89",
                name: "Documenti",
                y: iDOC
            },
            {
                color: "#79C3D6",
                name: "Altro",
                y: iOTH
            }],
            true
        );

    });
    myChart.series[0].update({name:"Q.tà"});

    $this.find(".modal-home-space").removeClass("loader").html(strHomeSpace);


}).on('hide.bs.modal', function (e) {
        var $this = $(this);

        // Show loader and hide folders size
        $this.find(".modal-folder-space").addClass("loader").html("&nbsp;");
        $this.find(".modal-parent-space").addClass("loader").html("&nbsp;");
        $this.find(".modal-home-space").addClass("loader").html("&nbsp;");
});


// Vertical centered modals
// you can give custom class like this // var modalVerticalCenterClass = ".modal.modal-vcenter";

var modalVerticalCenterClass = ".modal";
function centerModals($element) {
    var $modals;
    if ($element.length) {
        $modals = $element;
    } else {
        $modals = $(modalVerticalCenterClass + ':visible');
    }
    $modals.each( function(i) {
        var $clone = $(this).clone().css('display', 'block').appendTo('body');
        var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        $clone.remove();
        $(this).find('.modal-content').css("margin-top", top);
    });
}
$(modalVerticalCenterClass).on('show.bs.modal', function(e) {
    centerModals($(this));
});
$(window).on('resize', centerModals);

// Show App info
//$('#appInfo').modal('show');