'use strict';

const {app, BrowserWindow, Menu, MenuItem} = require('electron');

// Keep a global reference of the window object
let _WIN;

// Build window on app ready
app.on("ready", function(){
    // var main = new BrowserWindow({
    _WIN = new BrowserWindow({
        //maximizable: false,
        //fullscreen: false,
        //maxWidth: 990,
        //maxHeight: 800,
        //show: false,
        //useContentSize: true,
        //transparent: true,
        //frame: false,
        titleBarStyle: "hidden-inset",
        title: "My Custom App",
        width: 1000,
        height: 730,
        minWidth: 770,
        minHeight: 600
    });


    _WIN.loadURL("file://" + __dirname + "/index.html")
    // _WIN.webContents.openDevTools();

});


app.on('window-all-closed', app.quit);